### CMake version ###
CMAKE_MINIMUM_REQUIRED(VERSION 3.0 FATAL_ERROR)

### Project ###
PROJECT(fortjson LANGUAGES NONE)
SET(fortjson_URL "https://github.com/wphuhn/fortjson")
SET(fortjson_EMAIL "william.paul.huhn@gmail.com")
SET(fortjson_LICENSE "BSD 3")
SET(fortjson_DESCRIPTION "Library for outputting JSON files using Fortran 2003-compliant codes.  Part of the ELectronic Structure Infrastructure (ELSI) project.")
SET(fortjson_DATESTAMP "180331")

### Installation paths ###
IF(PROJECT_BINARY_DIR STREQUAL PROJECT_SOURCE_DIR)
  MESSAGE(FATAL_ERROR "Build in the source directory is not allowed")
ENDIF()

SET(CMAKE_Fortran_MODULE_DIRECTORY "${PROJECT_BINARY_DIR}/include")
SET(LIBRARY_OUTPUT_PATH "${PROJECT_BINARY_DIR}/lib")
SET(ARCHIVE_OUTPUT_PATH "${PROJECT_BINARY_DIR}/lib")
SET(EXECUTABLE_OUTPUT_PATH "${PROJECT_BINARY_DIR}/bin")
IF(NOT DEFINED CMAKE_INSTALL_INCLUDEDIR)
  SET(CMAKE_INSTALL_INCLUDEDIR "include" CACHE PATH "Location to install module and header files")
ENDIF()
IF(NOT DEFINED CMAKE_INSTALL_LIBDIR)
  SET(CMAKE_INSTALL_LIBDIR "lib" CACHE PATH "Location to install library files")
ENDIF()
IF(NOT DEFINED CMAKE_INSTALL_BINDIR)
  SET(CMAKE_INSTALL_BINDIR "bin" CACHE PATH "Location to install binary files")
ENDIF()
MESSAGE(STATUS "fortjson will be installed to: ${CMAKE_INSTALL_PREFIX}")

### Compilers ###
ENABLE_LANGUAGE(Fortran)
MESSAGE(STATUS "Fortran compiler: ${CMAKE_Fortran_COMPILER}")

### Compilations ###
INCLUDE_DIRECTORIES(${CMAKE_Fortran_MODULE_DIRECTORY})

add_subdirectory(doc)
ADD_SUBDIRECTORY(src)

ENABLE_TESTING()
ADD_CUSTOM_TARGET(check COMMAND ${CMAKE_CTEST_COMMAND})
ADD_SUBDIRECTORY(test)
