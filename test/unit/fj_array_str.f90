program fj_array_str

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: array with string value
   call fjson_open_file(fj_h, 66, "fj_array_str.json")
   call fjson_start_array(fj_h)
   call fjson_write_value(fj_h, "Placeholder Text")
   call fjson_finish_array(fj_h)
   call fjson_close_file(fj_h)

end program fj_array_str
