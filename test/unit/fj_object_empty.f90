program fj_object_empty

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: empty object
   call fjson_open_file(fj_h, 66, "fj_object_empty.json")
   call fjson_start_object(fj_h)
   call fjson_finish_object(fj_h)
   call fjson_close_file(fj_h)

end program fj_object_empty
