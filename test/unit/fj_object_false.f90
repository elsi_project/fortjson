program fj_object_false

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: object with a false literal name token in name/value pair
   call fjson_open_file(fj_h, 66, "fj_object_false.json")
   call fjson_start_object(fj_h)
   call fjson_write_name_value(fj_h, "Key", .false.)
   call fjson_finish_object(fj_h)
   call fjson_close_file(fj_h)

end program fj_object_false
