program fj_object_i4

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: object with integer(kind=i4) in name/value pair
   call fjson_open_file(fj_h, 66, "fj_object_i4.json")
   call fjson_start_object(fj_h)
   call fjson_write_name_value(fj_h, "Key", 1081985_i4)
   call fjson_finish_object(fj_h)
   call fjson_close_file(fj_h)

end program fj_object_i4
