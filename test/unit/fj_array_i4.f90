program fj_array_i4

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: array with integer(kind=i4) value
   call fjson_open_file(fj_h, 66, "fj_array_i4.json")
   call fjson_write_value(fj_h, (/ 0_i4, 1_i4, 2_i4, 3_i4, 4_i4 /), 5)
   call fjson_close_file(fj_h)

end program fj_array_i4
