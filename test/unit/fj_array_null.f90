program fj_array_null

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: array with null literal name token
   call fjson_open_file(fj_h, 66, "fj_array_null.json")
   call fjson_start_array(fj_h)
   call fjson_write_value(fj_h)
   call fjson_finish_array(fj_h)
   call fjson_close_file(fj_h)

end program fj_array_null
