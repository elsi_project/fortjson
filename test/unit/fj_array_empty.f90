program fj_array_empty

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: empty array
   call fjson_open_file(fj_h, 66, "fj_array_empty.json")
   call fjson_start_array(fj_h)
   call fjson_finish_array(fj_h)
   call fjson_close_file(fj_h)

end program fj_array_empty
