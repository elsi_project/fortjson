program fj_object_array_r4

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: object with name/value array containing real(kind=r4)
   call fjson_open_file(fj_h, 66, "fj_object_array_r4.json")
   call fjson_start_object(fj_h)
   call fjson_write_name_value(fj_h, "Key", &
                               (/ 0.01_r4, 0.08_r4, 0.85_r4 /), 3)
   call fjson_finish_object(fj_h)
   call fjson_close_file(fj_h)

end program fj_object_array_r4
