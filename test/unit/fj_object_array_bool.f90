program fj_object_array_bool

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: object with name/value array containing booleans
   call fjson_open_file(fj_h, 66, "fj_object_array_bool.json")
   call fjson_start_object(fj_h)
   call fjson_write_name_value(fj_h, "Key", &
                               (/ .true., .false., .true. /), 3)
   call fjson_finish_object(fj_h)
   call fjson_close_file(fj_h)

end program fj_object_array_bool
