program fj_r4

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: real(kind=r4)
   call fjson_open_file(fj_h, 66, "fj_r4.json")
   call fjson_write_value(fj_h, 0.1081985_r4)
   call fjson_close_file(fj_h)

end program fj_r4
