program fj_object_object

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: object with an (empty) object in name/value pair 
   call fjson_open_file(fj_h, 66, "fj_object_object.json")
   call fjson_start_object(fj_h)
   call fjson_start_name_object(fj_h, "Key")
   call fjson_finish_object(fj_h)
   call fjson_finish_object(fj_h)
   call fjson_close_file(fj_h)

end program fj_object_object
