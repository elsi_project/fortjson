program fj_object_array

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: object with an (empty) array in name/value pair 
   call fjson_open_file(fj_h, 66, "fj_object_array.json")
   call fjson_start_object(fj_h)
   call fjson_start_name_array(fj_h, "Key")
   call fjson_finish_array(fj_h)
   call fjson_finish_object(fj_h)
   call fjson_close_file(fj_h)

end program fj_object_array
