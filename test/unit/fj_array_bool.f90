program fj_array_bool

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: array with booleans
   call fjson_open_file(fj_h, 66, "fj_array_bool.json")
   call fjson_write_value(fj_h, (/ .true., .false., .true. /), 3)
   call fjson_close_file(fj_h)

end program fj_array_bool
