program fj_array_r4

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: array with real(kind=r4) value
   call fjson_open_file(fj_h, 66, "fj_array_r4.json")
   call fjson_write_value(fj_h, (/ 0.01_r4, 0.08_r4, 0.1985_r4 /), 3)
   call fjson_close_file(fj_h)

end program fj_array_r4
