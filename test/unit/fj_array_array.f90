program fj_array_array

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: array containing (empty) array
   call fjson_open_file(fj_h, 66, "fj_array_array.json")
   call fjson_start_array(fj_h)
   call fjson_start_array(fj_h)
   call fjson_finish_array(fj_h)
   call fjson_finish_array(fj_h)
   call fjson_close_file(fj_h)

end program fj_array_array
