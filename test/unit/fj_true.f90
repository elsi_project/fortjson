program fj_true

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: true literal name token
   call fjson_open_file(fj_h, 66, "fj_true.json")
   call fjson_write_value(fj_h, .true.)
   call fjson_close_file(fj_h)

end program fj_true
