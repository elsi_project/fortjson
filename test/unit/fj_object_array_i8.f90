program fj_object_array_i8

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: object with name/value array containing integer(kind=i8)
   call fjson_open_file(fj_h, 66, "fj_object_array_i8.json")
   call fjson_start_object(fj_h)
   call fjson_write_name_value(fj_h, "Key", &
                               (/ 0_i8, 1_i8, 2_i8, 3_i8, 4_i8 /), 5)
   call fjson_finish_object(fj_h)
   call fjson_close_file(fj_h)

end program fj_object_array_i8
