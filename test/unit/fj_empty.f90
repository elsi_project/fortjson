program fj_empty

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: empty
   call fjson_open_file(fj_h, 66, "fj_empty.json")
   call fjson_close_file(fj_h)

end program fj_empty
