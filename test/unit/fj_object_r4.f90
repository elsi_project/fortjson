program fj_object_r4

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: object with real(kind=r4) in name/value pair
   call fjson_open_file(fj_h, 66, "fj_object_r4.json")
   call fjson_start_object(fj_h)
   call fjson_write_name_value(fj_h, "Key", 0.1081985_r4)
   call fjson_finish_object(fj_h)
   call fjson_close_file(fj_h)

end program fj_object_r4
