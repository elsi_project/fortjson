program fj_null

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: null literal name token
   call fjson_open_file(fj_h, 66, "fj_null.json")
   call fjson_write_value(fj_h)
   call fjson_close_file(fj_h)

end program fj_null
