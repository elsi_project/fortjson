program fj_r8

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: real(kind=r8)
   call fjson_open_file(fj_h, 66, "fj_r8.json")
   call fjson_write_value(fj_h, 0.1081985_r8)
   call fjson_close_file(fj_h)

end program fj_r8
