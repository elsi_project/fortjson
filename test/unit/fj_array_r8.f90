program fj_array_r8

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: array with real(kind=r8) value
   call fjson_open_file(fj_h, 66, "fj_array_r8.json")
   call fjson_write_value(fj_h, (/ 0.01_r8, 0.08_r8, 0.1985_r8 /), 3)
   call fjson_close_file(fj_h)

end program fj_array_r8
