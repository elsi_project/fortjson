program fj_array_i8

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: array with integer(kind=i8) value
   call fjson_open_file(fj_h, 66, "fj_array_i8.json")
   call fjson_write_value(fj_h, (/ 0_i8, 1_i8, 2_i8, 3_i8, 4_i8 /), 5)
   call fjson_close_file(fj_h)

end program fj_array_i8
