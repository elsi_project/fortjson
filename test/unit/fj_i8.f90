program fj_i8

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: integer(kind=i8)
   call fjson_open_file(fj_h, 66, "fj_i8.json")
   call fjson_write_value(fj_h, 01081985_i8)
   call fjson_close_file(fj_h)

end program fj_i8
