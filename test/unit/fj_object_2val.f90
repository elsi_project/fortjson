program fj_object_2val

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: object with 2 name-value pairs
   call fjson_open_file(fj_h, 66, "fj_object_2val.json")
   call fjson_start_object(fj_h)
   call fjson_start_name_array(fj_h, "Key")
   call fjson_finish_array(fj_h)
   call fjson_write_name_value(fj_h, "Key2", "Placeholder Text")
   call fjson_finish_object(fj_h)
   call fjson_close_file(fj_h)

end program fj_object_2val
