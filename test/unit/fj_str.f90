program fj_str

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: string
   call fjson_open_file(fj_h, 66, "fj_str.json")
   call fjson_write_value(fj_h, "Placeholder Text")
   call fjson_close_file(fj_h)

end program fj_str
