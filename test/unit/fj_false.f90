program fj_false

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   ! JSON text: false literal name token
   call fjson_open_file(fj_h, 66, "fj_false.json")
   call fjson_write_value(fj_h, .false.)
   call fjson_close_file(fj_h)

end program fj_false
