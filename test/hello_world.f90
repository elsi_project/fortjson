program hello_world

   use FortJSON

   implicit none

   type(fjson_handle) :: fj_h

   integer(kind=i4) :: my_age
   real(kind=r8)    :: my_weight

   ! Open the file
   call fjson_open_file(fj_h, 66, "hello_world.json")
   call fjson_start_array(fj_h)

   call fjson_write_value(fj_h, "An array of pets")

   ! Rex the Dog
   my_age = 1_i4
   my_weight = 5.0_r8

   call fjson_start_object(fj_h)
   call fjson_write_name_value(fj_h, "My Age", my_age)
   call fjson_write_name_value(fj_h, "My Weight", my_weight)
   call fjson_write_name_value(fj_h, "My Name", "Rex Jr.")
   call fjson_write_name_value(fj_h, "Do I Know a Good Boy?", .false.)
   call fjson_write_name_value(fj_h, "Am I a Good Boy?", .true.)
   call fjson_write_name_value(fj_h, "Who's a Good Boy?", "Me")
   call fjson_write_name_value(fj_h, "Who's a Good Boy?", "It's me")
   call fjson_start_name_array(fj_h, "My Favorite Things")
   call fjson_write_value(fj_h, "Meat")
   call fjson_write_value(fj_h, "Cheese")
   call fjson_write_value(fj_h, "19th Century Fatalist Russian Literature")
   call fjson_write_value(fj_h, "Balls")
   call fjson_finish_array(fj_h)
   call fjson_start_name_object(fj_h, "My Favorite People")
   call fjson_write_name_value(fj_h, "Mom", "Precious")
   call fjson_write_name_value(fj_h, "Dad", "Rex")
   call fjson_write_name_value(fj_h, "Brother", "Spike")
   call fjson_write_name_value(fj_h, "Sister", "Jolene")
   call fjson_finish_object(fj_h)
   call fjson_finish_object(fj_h)

   ! Tiger the Cat
   my_age = 15_i4
   my_weight = 15.0_r8

   call fjson_start_object(fj_h)
   call fjson_write_name_value(fj_h, "My Age", my_age)
   call fjson_write_name_value(fj_h, "My Weight", my_weight)
   call fjson_write_name_value(fj_h, "My Name", "Tiger")
   call fjson_write_name_value(fj_h, "Do I Know a Good Boy?", &
                              "I don't care")
   call fjson_write_name_value(fj_h, "Am I a Good Boy?", &
                              "I don't care")
   call fjson_write_name_value(fj_h, "Who's a Good Boy?", &
                              "I don't care")
   call fjson_start_name_array(fj_h, "My Favorite Things")
   call fjson_finish_array(fj_h)
   call fjson_start_name_object(fj_h, "My Favorite People")
   call fjson_finish_object(fj_h)
   call fjson_finish_object(fj_h)

   call fjson_write_value(fj_h, &
                          fjson_error_message(fjson_get_last_error(fj_h)))

   ! Close the file
   call fjson_finish_array(fj_h)
   call fjson_close_file(fj_h)

end program hello_world
