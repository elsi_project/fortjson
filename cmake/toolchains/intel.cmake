# Location to install FortJSON to
SET(CMAKE_INSTALL_PREFIX "../install" CACHE STRING "Install location")

# Fortran 2003 compiler used for building FortJSON
SET(CMAKE_Fortran_COMPILER "ifort" CACHE STRING "Fortran compiler")

# Compilation flags to be used when building FortJSON
SET(CMAKE_Fortran_FLAGS "-O3" CACHE STRING "Fortran flags")
